package br.com.sulamerica.desafiosas;

import br.com.sulamerica.desafiosas.domain.Usuario;
import br.com.sulamerica.desafiosas.domain.UsuarioRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
public class DesafiosasApplication {
	private static final Logger logger = LoggerFactory.getLogger(DesafiosasApplication.class);
	@Autowired
	private UsuarioRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(DesafiosasApplication.class, args);
		// TODO: Implementar outros niveis de log
		logger.info("Aplicacao rodando!");
	}
	
	// TODO: Criar o script data.sql para definir a base e sua carga inicial
	@Bean
	CommandLineRunner runner() {
		return args -> {
			repository.save(new Usuario("Maria", "111.111.111.11", "F", "Secretaria"));
			repository.save(new Usuario("Joao", "222.222.222.22", "M", "Programador"));
			repository.save(new Usuario("Flavia", "333.333.333.33", "F", "Medica"));
		};
	}
}
