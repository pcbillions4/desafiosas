package br.com.sulamerica.desafiosas.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	// Pesquisa usuarios pelo nome
	List<Usuario> findByNome(String nome);

	// Pesquisa usuarios pelo Cargo
	List<Usuario> findByCargo(String cargo);

	// Pesquisa usuarios pelo CPF
	List<Usuario> findByCpf(String cpf);
}
